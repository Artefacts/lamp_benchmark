<?php

use App\Operation;
use App\Stuff;
use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Operation::TABLENAME, function (Blueprint $table) {
            $table->increments('id');
            $table->string('data1');
            $table->string('data2');
            $table->integer('user_id')->unsigned();
            $table->integer('stuff_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on(User::TABLENAME)->onDelete('cascade');
            $table->foreign('stuff_id')->references('id')->on(Stuff::TABLENAME)->onDelete('cascade');

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Operation::TABLENAME);
    }
}
