@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Stuffs</div>
                <div class="panel-body">

                <form method="POST" action="{!! route('addStuff') !!}">
                    {{ csrf_field() }}
                    <input type="text" name="data1" value="{{ str_random(191) }}" />
                    <input type="text" name="data2" value="{{ str_random(191) }}" />
                    <input type="submit" />
                </form>

                @php
                $stuff_last_id = 0 ;
                $ope_last_id = 0 ;
                @endphp
                @foreach( $stuffs as $stuff )
                    @php
                    $stuff_last_id = $stuff->id ;
                    @endphp
                    {{-- <p>{!! $stuff->id !!} {{ $stuff->data1 }} {{ $stuff->data2 }} --}}
                    @foreach( $stuff->operations as $ope )
                        @php
                        $ope_last_id = $ope->id ;
                        @endphp
                        {{-- <br/><code>{!! $ope->id !!} {{ $ope->data1 }} {{ $ope->data2 }}</code> --}}
                    @endforeach
                    {{-- </p>
                    <form method="POST" action="{!! route('addOperation', ['stuff'=>$stuff->id]) !!}">
                        {{ csrf_field() }}
                        <input type="text" name="data1" value="{{ str_random(191) }}" />
                        <input type="text" name="data2" value="{{ str_random(191) }}" />
                    <input type="submit" />
                    </form>
                    --}}
                @endforeach
                <span id="stuff_last_id" value="{!! $stuff_last_id !!}">stuff_last_id</span>
                <span id="ope_last_id" value="{!! $ope_last_id !!}" >ope_last_id</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
