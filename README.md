# LAMP benchmarck

nginx, php, mysql

1 ou plusieurs serveurs ?

# System & softwares

debian 10

```bash
sudo apt install mariadb-server nginx composer git php7.3-fpm php7.3-cli php7.3-mbstring php7.3-mysql php7.3-intl php7.3-xml php7.3-dom
```

# Mysql

default config

sudo mysqladmin -u root create homestead
sudo mysql homestead -e "GRANT ALL ON homestead.* TO 'homestead'@'%' IDENTIFIED BY 'secret';"

# Nginx

# Php

Php7.3 default configuration (opcache.enable => On, ...)

# Folders rights for Php-fpm
sudo chgrp -R www-data storage
sudo chmod -R g+w storage
sudo chgrp -R www-data bootstrap/cache
sudo chmod -R g+w bootstrap/cache

# Laravel
cp .env.example .env
./artisan key:generate

# before bench

reset-before-test.sh :
```
#!/bin/bash

DATE=`date '+%F_%H-%M'`

sudo mv /var/log/nginx/access.log /var/log/nginx/access.log.${DATE}
sudo mv /var/log/nginx/error.log /var/log/nginx/error.log.${DATE}
sudo systemctl restart php7.3-fpm.service
sudo systemctl restart nginx

sudo mv storage/logs/laravel.log storage/logs/laravel.log.${DATE}
# sudo: unable to execute /usr/bin/rm: Argument list too long
sudo rm storage/app/*.fake

./artisan config:clear
./artisan migrate:fresh
echo filling data...
# 1000 users, 4000 stuffs, 8000 operations
./artisan lb:usersCreate 1000
./artisan lb:stuffsCreate -D 4 2
```

## no log

.env
```
#APP_DEBUG=true
#APP_LOG_LEVEL=debug
APP_DEBUG=false
APP_LOG_LEVEL=error
```
nginx
```
error_log /dev/null ;
access_log /dev/null ;
```
