<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;

/**
 * @property Stuff[] $stuffs
 * @property Operation[] $operations
 */
class User extends Authenticatable
{
    use Notifiable;

    const TABLENAME = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','last_login_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'last_login_at'
    ];

    public function stuffs()
    {
        return $this->hasMany(Stuff::class);
    }

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }
}
