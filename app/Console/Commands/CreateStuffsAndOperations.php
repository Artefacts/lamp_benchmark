<?php

namespace App\Console\Commands;

use App\Operation;
use App\Stuff;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateStuffsAndOperations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lb:stuffsCreate'
        .' {stuffsCount : Stuff count to create per User}'
        .' {operationsCount : Operation count to create per Stuff}'
        .' {--D|drop : Drop all stuffs before}'
        ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if( $this->option('drop'))
        {
            DB::table(Stuff::TABLENAME)->delete();
        }

        $stuffsCount = $this->argument('stuffsCount');
        $operationsCount = $this->argument('operationsCount');

        foreach( User::all() as $user )
        {
            for( $i=1; $i<=$stuffsCount; $i++ )
            {
                $stuff = Stuff::create([
                    'user_id' => $user->id ,
                    'data1' => str_random(191),
                    'data2' => str_random(191),
                ]);
                for( $j=1; $j<=$operationsCount; $j++ )
                {
                    Operation::create([
                        'stuff_id' => $stuff->id ,
                        'user_id' => $user->id ,
                        'data1' => str_random(191),
                        'data2' => str_random(191),
                    ]);
                }
            }
        }

    }
}
