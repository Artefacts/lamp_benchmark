<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UsersCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lb:usersCreate'
        .' {count : Users count to create}'
        .' {--D|drop : Drop all users before}'
        ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create some users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if( $this->option('drop'))
        {
            //DB::table(User::TABLENAME)->truncate();
            DB::table(User::TABLENAME)->delete();
        }

        $n = $this->argument('count');

        for( $i=1; $i<=$n; $i++ )
        {
            User::create([
                'name' => 'user'.$i ,
	            'email' => 'user'.$i.'@internet.net',
	            'password' => \bcrypt('secret'),
            ]);
        }
    }
}
