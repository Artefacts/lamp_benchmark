<?php

namespace App\Http\Controllers;

use App\Operation;
use App\Stuff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * @var User $user
         */
        $user = Auth::user();
        return view('home',[
            'user' => $user,
            'stuffs' => $user->stuffs,
        ]);
    }

    public function addStuff( Request $request )
    {
        /**
         * @var User $user
         */
        $user = Auth::user();
        Log::info(__METHOD__,['user'=>$user->email]);
        $stuff = new Stuff([
            'data1' => $request->data1 ,
            'data2' => $request->data2 ,
        ]);
        $user->stuffs()->save($stuff);

        $filename = $user->id.'-'.microtime(true).'-'.$request->get('_token').'.fake';
        $fp = fopen( storage_path('app/'.$filename),'w');
        for( $i=0; $i<180; $i++ )
        {
            fwrite($fp,str_random(1000));
        }
        fclose($fp);

        return Redirect::route('home');
    }

    public function addOperation( Request $request, Stuff $stuff )
    {
        /**
         * @var User $user
         */
        $user = Auth::user();
        Log::info(__METHOD__,['user'=>$user->email]);

        $ope = new Operation([
            'data1' => str_random(191) ,
            'data2' => str_random(191) ,
            'user_id' => $user->id,
        ]);
        $stuff->operations()->save( $ope );

        return Redirect::route('home');
    }
}
