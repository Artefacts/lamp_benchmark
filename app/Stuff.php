<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $data1
 * @property string $data2
 */
class Stuff extends Model
{
    const TABLENAME = 'stuffs';
    /**
     * @var array
     */
    protected $fillable = [
        'data1', 'data2', 'user_id'
    ];
    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }

}
